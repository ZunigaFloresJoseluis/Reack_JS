![React](https://cdn-images-1.medium.com/max/800/1*XaGxIa_JuHc8YTR5Znv6tg.png)

# ReactJS

React JS es una tecnología, en realidad es una libreria desarrollada por 
facebook es de codigo abierto que esta desarrollado en JavaScritp para 
desarrollar interfaces.Fue desarrollado por Jordan Walke supuestamente basandose en XHP 
un marco de componentes  basado en php y html.
React no utiliza el patron de arquitectura MVC **Modelo vista controlador**
como angular si no en cambio esta orientado a la vista solamente


## Caracteristicas  
* React utiliza un Dom virtual, React realiza una copia del propio dom 
para que al realizar cambios solo compare los cambios y los haga efectivos
en las paginas.
    
* React tiene como objetivo realizar la programación mas facil que al utilizar
JQUERY o JavaScript puro
    
* Basado en componentes.
    
* React es isomorfo es decir  puede ser utilizado del lado del servidor como del
cliente 